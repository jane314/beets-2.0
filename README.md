![](beetlogo.png) 
# Beets 2.0

My own version of [Beets](https://beets.io/) written in Bash! Organize your tunes. 🎶🎼🎹🎙🥁

Relies on [jq](https://stedolan.github.io/jq/) and [ffmpeg](https://ffmpeg.org/). Works great with [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)!

The basic idea is: this will move your current working directory's audio files to an organized, standardized hierarchy like:

`/BASE_DIRECTORY/ARTIST_NAME/ALBUM_NAME-TRACK_NUMBER-ARTIST_NAME-TRACK_TITLE.mp3`.

For example, if your base directory is `/home/janie/music`, a typical file could be:

`/home/janie/music/BLACK_DRESSES/HELL_IS_REAL-04-BLACK_DRESSES-theres_a_halo_around_the_moon.flac`.

The input and output directories are specified in `beets2config.json`. Do not use trailing slashes in these paths! Use `/this/format`, not `/this/format/`.

Currently, this script searches the music files underneath your current working directory, and outputs the Bash script that would move organize these files into `./beets2script.sh`. It doesn't actually execute the commands to organize your files, because I would hate to mess up anyone's music library, and this hasn't been tested much!

## WSL Tips:

* The WSL `/` Directory can be accessed in the `\\wsl$` path in Windows.
* This is the command to mount the Windows `E:\` drive at `/mnt/e/` in WSL: `sudo mount -t drvfs E: /mnt/e`.
* To convert Windows filepaths to WSL: `sed -e "s/C:/\/mnt\/c/g" -e "s/\\\/\//g"`.

## Sample Output Directory:

![](screenshot.png)
